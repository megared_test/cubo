<?php
	require_once("db.php");
	require_once("../objects/Cube.php");

	/* 
	DAO para las acciones sobre la tabla Cube
	*/
	Class CubeDAO {

		public function deleteAll(){
			$query = "TRUNCATE TABLE Cube;";
			$db = new DB();
			$result = $db->executeQuery($query);
		}

		public function create($N){
			$db = new DB();
			for ($x = 1; $x <= $N; $x++) {
	            for ($y = 1; $y <= $N; $y++) {
	                for ($z = 1; $z <= $N; $z++) {
	                    $query = "INSERT INTO Cube (x,y,z,W) VALUES (".$x.", ".$y.", ".$z.", 0);";
						$result = $db->executeQuery($query);
	                }
	            }
	        }
	        return true;
		}

		public function update($x, $y, $z, $W){
			$query = "UPDATE Cube SET W = ".$W." WHERE x = ".$x." AND y = ".$y." AND z = ".$z.";";
			$db = new DB();
			$result = $db->executeQuery($query);
			if($result != 0){
				return true;
			}
			return false;
		}

		public function query($x1, $y1, $z1, $x2, $y2, $z2){
			$query = "	SELECT SUM(W) as Suma
						FROM cube_summation.`cube` 
						WHERE id >= (SELECT id FROM cube_summation.`cube` WHERE x = ".$x1." AND y = ".$y1." AND z = ".$z1.") 
						 AND id <= (SELECT id FROM cube_summation.`cube` WHERE x = ".$x2." AND y = ".$y2." AND z = ".$z2.") 
						;";
			$db = new DB();
			$result = $db->executeSelectQuery($query);
			if($result != null){
				return $result[0]["Suma"];
			}
			return -1;
		}
		
	}
?>