<?php
	/* 
	DTO para la tabla CubeTrace
	*/
	Class CubeTrace {
		
		private $ip;
		private $query;
		private $result;

		public function setIp($ipParam){
			$this->ip = $ipParam;
		}

		public function setQuery($queryParam){
			$this->query = $queryParam;
		}

		public function setResult($resultParam){
			$this->result = $resultParam;
		}

		public function getIp(){
			return $this->ip;
		}

		public function getQuery(){
			return $this->query;
		}

		public function getResult(){
			return $this->result;
		}
	}
?>
