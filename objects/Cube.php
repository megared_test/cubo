<?php
	/* 
	DTO para la tabla Cube
	*/
	Class Cube {
		
		private $x;
		private $y;
		private $z;
		private $w;

		public function setX($xParam){
			$this->x = $xParam;
		}

		public function setY($yParam){
			$this->y = $yParam;
		}

		public function setZ($zParam){
			$this->z = $zParam;
		}

		public function setW($wParam){
			$this->w = $wParam;
		}

		public function getX(){
			return $this->x;
		}

		public function getY(){
			return $this->y;
		}

		public function getZ(){
			return $this->z;
		}

		public function getW(){
			return $this->w;
		}
	}
?>