<?php
	require_once("MobileRestHandler.php");
	require_once("../objects/CubeTrace.php");
	require_once("../models/CubeTraceDAO.php");
	require_once("../models/CubeDAO.php");
			
	$action = "";
	if(isset($_GET["action"]))
		$action = $_GET["action"];

	$cubeTrace = new CubeTrace();
	if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
	    $ip = $_SERVER['HTTP_CLIENT_IP'];
	} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
	    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
	} else {
		$ip = $_SERVER['REMOTE_ADDR'];
	}
	$cubeTrace->setIp($ip);

	$cube = new CubeDAO();
			
	switch($action){

		case "CREATE":
			if(isset($_GET["N"])){
				$cubeTrace->setQuery("CREATE ".$_GET["N"]);

				$cube->deleteAll();
				if($cube->create($_GET["N"])){
					$cubeTrace->setResult("OK");
					echo "OK";
				} else {
					$cubeTrace->setResult("Error");
					echo "Se generó un error en la acción CREATE";
				}

				$cubeTraceDAO = new CubeTraceDAO();
				$cubeTraceDAO->addTrace($cubeTrace);
			} else {
				echo "Los parámetros no son correctos, ejemplo de uso: action=CREATE&N=2";
			}
			break;

		case "UPDATE":
			if(isset($_GET["x"]) && isset($_GET["y"]) && isset($_GET["z"]) && isset($_GET["W"])) {
				$cubeTrace->setQuery("UPDATE ".$_GET["x"]." ".$_GET["y"]." ".$_GET["z"]." ".$_GET["W"]);

				if($cube->update($_GET["x"], $_GET["y"], $_GET["z"], $_GET["W"])){
					$cubeTrace->setResult("OK");
					echo "OK";
				} else {
					$cubeTrace->setResult("Error");
					echo "Se generó un error en la acción UPDATE";
				}

				$cubeTraceDAO = new CubeTraceDAO();
				$cubeTraceDAO->addTrace($cubeTrace);
			} else {
				echo "Los parámetros no son correctos, ejemplo de uso: action=UPDATE&x=1&y=1&z=1&W=3";
			}
			break;

		case "QUERY":
			if(isset($_GET["x1"]) && isset($_GET["y1"]) && isset($_GET["z1"]) && isset($_GET["x2"]) && isset($_GET["y2"]) && isset($_GET["z2"])) {
				$cubeTrace->setQuery("QUERY ".$_GET["x1"]." ".$_GET["y1"]." ".$_GET["z1"].$_GET["x2"]." ".$_GET["y2"]." ".$_GET["z2"]);
				
				$result = $cube->query($_GET["x1"], $_GET["y1"], $_GET["z1"], $_GET["x2"], $_GET["y2"], $_GET["z2"]);
				if($result != -1){
					$cubeTrace->setResult($result);
					echo $result;
				} else {
					$cubeTrace->setResult("Error");
					echo "Se generó un error en la acción QUERY";
				}

				$cubeTraceDAO = new CubeTraceDAO();
				$cubeTraceDAO->addTrace($cubeTrace);
			} else {
				echo "Los parámetros no son correctos, ejemplo de uso: action=QUERY&x1=1&y1=1&z1=1&x2=2&y2=2&z2=2";
			}
			break;

		case "" :
			//404 - not found;
			break;
	}
?>
